class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :status
      t.integer :order
      t.datetime :duedate
      t.string :location
      t.text :description

      t.timestamps null: false
    end
  end
end
