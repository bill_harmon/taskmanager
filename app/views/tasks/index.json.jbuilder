json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :status, :order, :duedate, :location, :description
  json.url task_url(task, format: :json)
end
